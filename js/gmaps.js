console.log("GMAPS LOADED");
// Custom Draw Map Function
var directionsService = null,
    directionsDisplay = null,
    directionEnabled = true,
    openMarkers = true;

define([
    //dependencies
    'mustache'
], function (Mustache) {
    return {
        /**
         * Draw the map for the given data and parameters
         * @constructor
         * @param {string} data       : 1st parameter - Map data with coordinates, name and so on.
         * @param {string} mapId      : 2st parameter - Map ID. Where to draw map.
         * @param {number} zoomLevel  : 3nd Parameter - Zoom loevel of the map.
         * @param {string} template   : 4th Parameter - Info window template.
         * @param {boolean} direction : 5th Parameter - Draw the route - true or false.
         * @param {boolean} openMarker: 6th Parameter - Onload open markers - true or false.
         * @param {string} iconUrl    : 7rd Parameter - Marker icon current position of client.
         * @param {string} icons      : 8th Parameter - Icons object, icon types and icon paths for markers.
         */
        DrawMap: function (data, mapId, zoomLevel, template, direction, openMarker, iconUrl, icons) {

            function initialize(data, mapId, zoomLevel, template, direction, openMarker, iconUrl, icons) {
                if (navigator.geolocation) {
                    var mapSelect = document.getElementById(mapId);
                    navigator.geolocation.getCurrentPosition(function (position) {

                        // Map Options, Map Init, Current Position Marker and InfoWidnow
                        var i, _infowin = null,
                            currentPos = {lat: position.coords.latitude, lng: position.coords.longitude},
                            mapOptions = {
                                center: new google.maps.LatLng(currentPos.lat, currentPos.lng),
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                disableDefaultUI: true,
                                zoom: zoomLevel
                            },
                            map = new google.maps.Map(mapSelect, mapOptions),
                            currentPosMarker = new google.maps.Marker({
                                position: new google.maps.LatLng(currentPos.lat, currentPos.lng),
                                travelMode: 'driving',
                                icon: iconUrl,
                                map: map
                            }),
                            currentPosInfoW = new google.maps.InfoWindow({
                                content: 'Konumunuz',
                                position: new google.maps.LatLng(currentPos.lat, currentPos.lng)
                            });

                        if (openMarkers == openMarker) {
                            currentPosInfoW.open(map, currentPosMarker);
                        }

                        // Current Position Marker Info Window Click Listener
                        google.maps.event.addListener(currentPosMarker, 'click', function () {
                            currentPosInfoW = new google.maps.InfoWindow({
                                content: 'Konumunuz',
                                position: new google.maps.LatLng(currentPos.lat, currentPos.lng)
                            });
                            currentPosInfoW.open(map);
                        });

                        // Place markers and event listeners.
                        for (i = 0; i < data.length; ++i) {
                            (function () {
                                if (i >= 24) return;
                                var latLng = new google.maps.LatLng(data[i].lng.trim().replace(",", "."), data[i].lat.trim().replace(",", ".")),
                                    locationType = data[i].type,
                                    iconType = icons[locationType].icon,
                                    marker = new google.maps.Marker({
                                        position: latLng,
                                        icon: iconType,
                                        map: map
                                    }),
                                    result = Mustache.render(template, data[i]),
                                    infowindow = new google.maps.InfoWindow({
                                        content: Mustache.render(template, data[i]) // Render infowindow template.
                                    });

                                if (openMarkers == openMarker) { // On load open or closed info infowindow
                                    infowindow.open(map, marker);
                                }

                                google.maps.event.addListener(marker, 'click', function () {
                                    // Add click listener to draw routes
                                    if (directionsDisplay)
                                        directionsDisplay.setMap(null);
                                    directionsService = new google.maps.DirectionsService();
                                    directionsDisplay = new google.maps.DirectionsRenderer();
                                    directionsDisplay.setMap(map);
                                    directionsDisplay.setOptions({suppressMarkers: true});

                                    var request = {
                                        origin: new google.maps.LatLng(currentPos.lat, currentPos.lng),
                                        destination: marker.position,
                                        travelMode: google.maps.DirectionsTravelMode.WALKING
                                    };

                                    // Whether draw route or not.
                                    if (directionEnabled == direction) {
                                        directionsService.route(request, function (response, status) {
                                            if (status == google.maps.DirectionsStatus.OK) {
                                                directionsDisplay.setDirections(response);
                                            }
                                        });
                                    }

                                    if (_infowin != null)
                                        _infowin.close();
                                    _infowin = infowindow;
                                    _infowin.open(map, marker);
                                });

                                google.maps.event.addListener(infowindow, 'domready', function () {
                                    //** Styling Info Window. Remove comment if you want to style the info windows
                                    // var iwOuter = document.querySelector(".gm-style-iw");
                                    // var parentIw = iwOuter.parentNode;
                                    // parentIw.className = "infoWrapper";

                                    //** Dom Ready hide loader
                                    document.getElementById("loader").style.display = 'none';
                                });
                            })();
                        }
                    }, function () {
                        handleLocationError(true);
                    }, {timeout: 10000});
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false);
                }
                function handleLocationError() {
                    alert("Lütfen cihazınızın konum hizmetlerini açınız!")
                }
            }

            google.maps.event.addDomListener(window, 'load', initialize(data, mapId, zoomLevel, template, direction, openMarker, iconUrl, icons));
        }
    };
});