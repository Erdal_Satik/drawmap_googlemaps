console.log("MAIN JS LOADED");
//Dependency & App Logic
requirejs.config({
    baseUrl: 'js/',
    paths: {
        mustache: 'mustache',
        gmaps: 'gmaps'
    }
});

// App logic.
requirejs(['mustache', 'gmaps'],
    function (Mustache, gmaps) {
        console.log("ready");

        var dataBakery = [
                {"type":"bakery", "name":"Hamurcu Baba", "address":" Hamurcu Baba <br> Maslak 34752, İstanbul ","lng":" 41.112585 ","lat":" 29.019065 ", "ratio":"0.55"},
                {"type":"store", "name":"Mağaza Dükkan", "address":" Sıcak Fırınım <br> Maslak 34752, İstanbul ","lng":" 41.113985 ","lat":" 29.022485 ", "ratio":"0.85"},
                {"type":"bakery", "name":"Sadece Ekmek", "address":" Hamura Gel Fırınım <br> Maslak 34752, İstanbul ","lng":" 41.110985 ","lat":" 29.018795 ", "ratio":"0.25"},
                {"type":"shopping", "name":"Alışveriş AVM", "address":" HammurAbi <br> Maslak 34752, İstanbul ","lng":" 41.112005 ","lat":" 29.022865 ", "ratio":"0.15"},
                {"type":"bakery", "name":"Hamur Fırın", "address":" Pidenin Güzeli <br> Maslak 34752, İstanbul ","lng":" 41.114085 ","lat":" 29.020005 ", "ratio":"0.95"},
                {"type":"bakery", "name":"Lembas Peksimet Fırın", "address":" Pidenin Güzeli <br> Maslak 34752, İstanbul ","lng":" 41.108485 ","lat":" 29.021205 ", "ratio":"0.95"}
            ],

        /* Info Window Template */
            bakeryTemplate = '<div class="infoWindow">' +
                '{{#.}}' +
                '<b>{{name}}</b><br/>' +
                '<span>{{ratio}}</span>' +
                '{{/.}}' +
                '</div>';

        /* Icons Types */
        var icons = {
            store: {
                icon:'img/store.png'
            },
            bakery: {
                icon:'img/bread.png'
            },
            shopping: {
                icon:'img/shopping.png'
            }
        };

        /* Parameters : data, mapId, zoomLevel, template, direction(true,false), openMarkers (true or false), iconUrl, icons */
        new gmaps.DrawMap(dataBakery, 'map-canvas', 16, bakeryTemplate, false, false, 'img/marker3.png', icons);

    }
);